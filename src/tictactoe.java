import java.util.Scanner;

public class tictactoe {

	static Scanner kb;
	static String[][] board = new String[3][3];
	static String turn;
	static String winner = null;
	static int num;
	static int inputR, inputC;

	public static void main(String[] args) {
		kb = new Scanner(System.in);
		startBoard();
		check();
		checkDraw(winner);

	}

	static void setInt() {
		inputR = 0;
		inputC = 0;
	}

	static void startBoard() {
		turn = "X";
		System.out.println("Welcome to Tic Tac Toe.\n");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board[i][j] = "-";
			}
		}
		printBoard();
		System.out.print(turn + "turn." + "Please input row and column here: ");
	}

	static void printBoard() {
		System.out.println("     " + "1  " + " 2  " + " 3  ");
		System.out.println();
		System.out.println("1    " + board[0][0] + "   " + board[0][1] + "   " + board[0][2]);
		System.out.println();
		System.out.println("2    " + board[1][0] + "   " + board[1][1] + "   " + board[1][2]);
		System.out.println();
		System.out.println("3    " + board[2][0] + "   " + board[2][1] + "   " + board[2][2]);
		System.out.println();
	}

	static void check() {
		while (winner == null) {
			inputR = kb.nextInt();
			inputC = kb.nextInt();
			if (inputR == 1 && inputC == 1 && board[0][0].equals("-")) {
				board[0][0] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 1 && inputC == 2 && board[0][1].equals("-")) {
				board[0][1] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 1 && inputC == 3 && board[0][2].equals("-")) {
				board[0][2] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 2 && inputC == 1 && board[1][0].equals("-")) {
				board[1][0] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 2 && inputC == 2 && board[1][1].equals("-")) {
				board[1][1] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 2 && inputC == 3 && board[1][2].equals("-")) {
				board[1][2] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 3 && inputC == 1 && board[2][0].equals("-")) {
				board[2][0] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 3 && inputC == 2 && board[2][1].equals("-")) {
				board[2][1] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else if (inputR == 3 && inputC == 3 && board[2][2].equals("-")) {
				board[2][2] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else
					turn = "X";
				printBoard();
				winner = checkWinner();
				num++;
				setInt();
			} else {
				System.out.print("re-enter slot number: ");
				setInt();
				continue;
			}

		}
	}

	static String checkWinner() {
		for (int a = 0; a < 8; a++) {
			String line = null;
			switch (a) {
			case 0:
				line = board[0][0] + board[0][1] + board[0][2];
				break;
			case 1:
				line = board[1][0] + board[1][1] + board[1][2];
				break;
			case 2:
				line = board[2][0] + board[2][1] + board[2][2];
				break;
			case 3:
				line = board[0][0] + board[1][0] + board[2][0];
				break;
			case 4:
				line = board[0][1] + board[1][1] + board[2][1];
				break;
			case 5:
				line = board[0][2] + board[1][2] + board[2][2];
				break;
			case 6:
				line = board[0][0] + board[1][1] + board[2][2];
				break;
			case 7:
				line = board[2][0] + board[1][1] + board[0][2];
				break;
			}
			if (line.equals("XXX")) {
				return "X";
			} else if (line.equals("OOO")) {
				return "O";
			}
		}

		for (int a = 0; a < 9; a++) {
			if (num >= 8)
				return "draw";
		}

		System.out.print(turn + "[R,C]: ");
		return null;
	}

	static void checkDraw(String winner) {
		if (winner.equalsIgnoreCase("draw")) {
			System.out.println("It's a draw! Thanks for playing.");
			setInt();
		} else {
			System.out.println("Congratulations! " + winner + "'s have won! Thanks for playing.");
			setInt();
		}
	}

}
